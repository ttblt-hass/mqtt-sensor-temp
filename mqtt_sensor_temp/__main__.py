"""Module defining entrypoint."""
import asyncio

from mqtt_sensor_temp import device


def main():
    """Entrypoint function."""
    dev = device.MqttSensorTemp()
    asyncio.run(dev.async_run())


if __name__ == "__main__":
    main()
