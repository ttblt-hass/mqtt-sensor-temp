"""Mqtt Kab switch module."""
import asyncio
import glob
import json
import os
import time
import socket
import sys
import uuid

import mqtt_hass_base

from mqtt_sensor_temp.__version__ import VERSION


MAIN_LOOP_WAIT_TIME = 60

DEVICE_CLASSES = (None,  # Generic sensor. This is the default and doesn’t need to be set.
                  "battery",  # Percentage of battery that is left.
                  "humidity",  # Percentage of humidity in the air.
                  "illuminance",  # The current light level in lx or lm.
                  "signal_strength",  # Signal strength in dB or dBm.
                  "temperature",  # Temperature in °C or °F.
                  "power",  # Power in W or kW.
                  "pressure",  # Pressure in hPa or mbar.
                  "timestamp",  # Datetime object or timestamp string.
                  )


def get_mac():
    """Get mac address."""
    mac_addr = (':'.join(['{:02x}'.format((uuid.getnode() >> ele) & 0xff)
                for ele in range(0, 8 * 6, 8)][::-1]))
    return mac_addr


# https://www.raspberrypi-spy.co.uk/2012/09/checking-your-raspberry-pi-board-version/
def get_raspi_model():
    """Get Raspberry Pi model."""
    model = None
    if os.path.isfile('/proc/device-tree/model'):
        with open("/proc/device-tree/model", 'rb') as fhm:
            model = fhm.read()
    model = model.strip(b'\x00').decode('utf-8')
    return model


def get_raspi_serial():
    """Get Raspberry Pi serial."""
    serial = None
    if os.path.isfile('/proc/device-tree/serial-number'):
        with open("/proc/device-tree/serial-number", 'rb') as fhs:
            serial = fhs.read()
    serial = serial.strip(b'\x00').decode('utf-8')
    return serial


def get_hostname():
    """Get Hostname."""
    return socket.gethostname()


def _read_temp_raw():
    """Read temp device."""
    base_dir = '/sys/bus/w1/devices/'
    device_folder = glob.glob(base_dir + '28*')[0]
    device_file = device_folder + '/w1_slave'

    with open(device_file, 'r') as fhd:
        lines = fhd.readlines()
    return lines


def _pool_temp(c_offset=0):
    """Get temperature."""
    lines = _read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = _read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_c += c_offset
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c, temp_f


class MqttSensorTemp(mqtt_hass_base.MqttDevice):
    """MQTT Sensor Temp for Raspberry PI."""

    def __init__(self):
        """Constructor."""
        mqtt_hass_base.MqttDevice.__init__(self, "mqtt-sensor-temp")

    def read_config(self):
        """Read env vars."""
        self.device_name = os.environ['DEVICE_NAME']
        self.c_offset = float(os.environ.get('C_OFFSET', 0))
        self.device_class = os.environ.get('DEVICE_CLASS', 'temperature')
        if self.device_class not in DEVICE_CLASSES:
            self.logger.error("Device classe should be: %s", ",".join(DEVICE_CLASSES))
            sys.exit(1)
        self.force_update = bool(int(os.environ.get('FORCE_UPDATE', 1)))
        self.expire_after = int(os.environ.get("EXPIRE_AFTER", 0))

    async def _init_main_loop(self):
        """Init before starting main loop."""

    async def _main_loop(self):
        """Run main loop."""
        self.logger.debug("Get Data")
        topic_p_base = ("{}/binary_sensor/{}/{}".format(self.mqtt_root_topic,
                                                        self.name,
                                                        self.device_name.lower().replace(" ", "-")))
        topic_t_base = ("{}/sensor/{}/{}".format(self.mqtt_root_topic,
                                                 self.name,
                                                 self.device_name.lower().replace(" ", "-")))
        temp_c, temp_f = _pool_temp(self.c_offset)
        self.logger.debug("temp_c: %s # temp_f %s", temp_c, temp_f)
        self.mqtt_client.publish(topic="{}/state".format(topic_p_base),
                                 payload="ON")
        self.mqtt_client.publish(topic="{}/temperature_c".format(topic_t_base),
                                 payload="{:0.2f}".format(temp_c))
        self.mqtt_client.publish(topic="{}/temperature_f".format(topic_t_base),
                                 payload="{:0.2f}".format(temp_f))

        self.logger.debug("Data send")
        i = 0
        while i < MAIN_LOOP_WAIT_TIME and self.must_run:
            await asyncio.sleep(1)
            i += 1

    def _on_connect(self, client, userdata, flags, rc):
        """MQTT on connect callback."""
        config = {}
        mac_addr = get_mac()
        hostname = get_hostname()
        config["device"] = {"connections": [["mac", mac_addr]],
                            "name": hostname,
                            "sw_version": VERSION}

        model = get_raspi_model()
        if model is not None:
            config["device"]["model"] = model

        serial = get_raspi_serial()
        if serial is not None:
            config["device"]["identifiers"] = serial

        # Presence
        topic_p_base = ("{}/binary_sensor/{}/{}".format(self.mqtt_root_topic,
                                                        self.name,
                                                        self.device_name.lower().replace(" ", "-")))
        config_p = config.copy()
        config_p.update({"state_topic": "{}/state".format(topic_p_base),
                         "name": self.device_name,
                         "device_class": "connectivity",
                         "payload_on": "ON",
                         "payload_off": "OFF",
                         "off_delay": self.expire_after,
                         })
        mqtt_config_p_topic = "{}/config".format(topic_p_base)
        self.logger.debug("%s: %s", mqtt_config_p_topic, json.dumps(config_p))
        self.mqtt_client.publish(topic=mqtt_config_p_topic,
                                 retain=True,
                                 payload=json.dumps(config_p))
        # Temperature C
        topic_t_base = ("{}/sensor/{}/{}".format(self.mqtt_root_topic,
                                                 self.name,
                                                 self.device_name.lower().replace(" ", "-")))
        config_c = config.copy()
        config_c.update({"state_topic": "{}/temperature_c".format(topic_t_base),
                         "name": self.device_name + " C",
                         "unit_of_measurement": "C",
                         "force_update": self.force_update,
                         })
        config_c["device_class"] = self.device_class
        config_c["expire_after"] = self.expire_after
        mqtt_config_c_topic = ("{}/sensor/{}/{}/"
                               "config".format(self.mqtt_root_topic,
                                               self.name,
                                               self.device_name.lower().replace(" ", "-")))
        self.logger.debug("%s: %s", mqtt_config_c_topic, json.dumps(config_c))
        self.mqtt_client.publish(topic=mqtt_config_c_topic,
                                 retain=True,
                                 payload=json.dumps(config_c))
        # Temperature F
        config_f = config_c.copy()
        config_f["device"]["name"] += "_F"
        config_f["state_topic"] = "{}/temperature_f".format(topic_t_base)
        config_f["name"] = self.device_name + " F"
        config_f["unit_of_measurement"] = "F"
        mqtt_config_f_topic = ("{}/sensor/{}/{}-f/"
                               "config".format(self.mqtt_root_topic,
                                               self.name,
                                               self.device_name.lower().replace(" ", "-")))
        self.logger.debug("%s: %s", mqtt_config_f_topic, json.dumps(config_f))
        self.mqtt_client.publish(topic=mqtt_config_f_topic,
                                 retain=True,
                                 payload=json.dumps(config_f))

    def _on_publish(self, client, userdata, mid):
        """MQTT on publish callback."""

    def _mqtt_subscribe(self, client, userdata, flags, rc):
        """Subscribe to all needed MQTT topic."""

    def _on_message(self, client, userdata, msg):
        """MQTT on message callback."""

    def _signal_handler(self, signal_, frame):
        """Handle SIGKILL."""

    async def _loop_stopped(self):
        """Run after the end of the main loop."""
