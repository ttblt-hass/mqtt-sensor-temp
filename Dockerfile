FROM arm32v6/alpine

RUN apk add --no-cache python3 py-pip

RUN mkdir -p /app
WORKDIR /app

COPY Dockerfile /Dockerfile
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY requirements.txt  setup.py  test_requirements.txt /app/
RUN pip3 install -r /app/requirements.txt

COPY mqtt_sensor_temp /app/mqtt_sensor_temp
RUN python3 setup.py install

ENV MQTT_USERNAME=username
ENV MQTT_PASSWORD=password
ENV MQTT_HOST=192.168.0.1
ENV MQTT_PORT=1883
ENV ROOT_TOPIC=homeassistant
ENV MQTT_NAME=sensortemp
ENV LOG_LEVEL=INFO
ENV C_OFFSET=0

CMD sh /entrypoint.sh
