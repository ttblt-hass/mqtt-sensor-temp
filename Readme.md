## Run it locally

Run it:
```
pip install -r requirements.txt

MQTT_USERNAME=hass \
	MQTT_PASSWORD=hass \
	MQTT_HOST=192.168.0.1 \
	MQTT_PORT=1883 \
	ROOT_TOPIC=homeassistant \
	MQTT_NAME=sensortemp \
	LOG_LEVEL=DEBUG \
	env/bin/mqtt_sensor_temp

```

## Build Docker image

```
docker build -t registry.gitlab.com/ttblt-hass/mqtt-sensor-temp:armv6-0.1.1 .
```

## Run Docker container

```
docker run --rm -it \
    --name mqttsensortemp \
    -e MQTT_USERNAME=hass \
    -e MQTT_PASSWORD=hass \
    -e MQTT_HOST=192.168.0.1 \
    -e MQTT_PORT=1883 \
    -e ROOT_TOPIC=homeassistant \
	-e MQTT_NAME=sensortemp \
    -e LOG_LEVEL=INFO \
    mqtt-sensor-temp:latest bash
```
